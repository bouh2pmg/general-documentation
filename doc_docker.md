---
module:                 Epitech Documentation
title:                  How to test your project
subtitle:               with our test docker

author:                 Alexandre Vanhecke
version:                1.0

noFormalities:          true
---

# How to run your project into our test docker

## Install docker

You can find many ways to install docker, take a look at [installation guide](https://docs.docker.com/install/#supported-platforms)#br

#warn(You have to run any Linux distribution to work with our docker image)

## Download our docker from dockerhub

It's pretty easy to download our docker image from dockerhub, after successfully installing Docker on your system, you can 'docker pull' our image :#br

#terminalNoPrompt(docker pull epitechcontent/epitest-docker:latest)

#newpage

## Prepare the script to run your project

In case you're running docker on a linux system, you can use this script template and adapt it for your current project#br

```bash
#!/bin/bash

# Specify folder where you can find sources, on your local machine
SOURCE_FOLDER=/home/me/epitech/my_project/

# Specify destination folder to mount your project into docker
DEST_FOLDER=/home/student/

docker run --rm -v "$SOURCE_FOLDER:$DEST_FOLDER" -it epitechcontent/epitest-docker /bin/bash -c 'useradd student && su - student'
```

Save it as a runnable shell script (like `run.sh`) and launch it#br

You'll be granted a command prompt, from where you can run some commands :#br

#terminalNoPrompt([student ~]$ make
[student ~]$ ./my_project
[student ~]$ ./tests/my_own_tests_suite)

That's it, you can now easily run any tests suite inside your docker container#br

#warn(In case your project doesn't compile nor run, please take a few steps back and check if you missed something above.)

#warn(You can still install other packages on the docker container, but our automated test docker does not have any internet access except for projects with some dependencies manager (maven, gradle, npm, conan, etc..))
