---
module:			Epitech Documentation
title:			Pedagogical Activities
subtitle:		How to pedagogically conduct a unit

author:			Jonathan Nau / Pierre Robert
version:		1.6.0
noFormalities:	true
---

Units are mainly composed by 4 different pedagogical activities, derived from the professional life cycle of a project.
As an example, read carefully the following 5 phases of a well-managed project, *from an external source*:#br

#imageCenter(images/5-phases.png, 400px)

These 4 activities are:

- **Kick-off** (project definition and introduction) *[phases 1&2 in the previous example]*

- **Bootstrap** (project launching) *[phase 3 in the previous example]*

- **Follow-up** (project controlling) *[phase 4 in the previous example]*

- **Review** and **Code Review** (project reporting) *[phase 5 in the previous example]*

They are all related to a given project (except for the unit kick-off), and must be timed according to the project's key dates:

- the project starting date,
- the deadline for students registration,
- the delivery date (typically, on Sunday at 11:42PM).

#hint(These elements are available on the intranet.)

Most projects are automatically tested right after the delivery date: once the delivery date has passed, students' repositories are cloned into their final deliveries which will serve for the project evaluation. 
No file sent after this date will be taken into account in this process.

#warn(Grades are available to pedagogical staff on [Argos](https://argos.epitest.eu/).
Students receive an email with the results and more detailed logs are available to pedagogical staff on [Jenkins](https://jenkins.epitest.eu/).)





#newpage

# Kick-off
+--------------------------------------------------------------------------------------------------------------------------------------------------------------
| #color(COLOR_red,**planning**) 	#space(.58) 	at the very beginning of the unit/project\, ideally on **Monday**
| #color(COLOR_red,**duration**)	#space(.6) 		**30 minutes** (up to 1 hour if necessary)
| #color(COLOR_red,**staff**)		#space(1.25) 	**senior** enough to have the necessary perspective and to be able to speak to a wide audience
| #color(COLOR_red,**audience**)	#space(.5) 		up to **100 students**
| #color(COLOR_red,**room**)		#space(1.1) 	**lecture hall**
| #color(COLOR_red,**intranet**)	#space(.6) 		**subscription**, no project, no mark, no time-slot, no rating scale
+--------------------------------------------------------------------------------------------------------------------------------------------------------------#br

There are two types of kick-offs: unit kick-offs (1 per unit) and project kick-offs (1 per project).
Kick-offs' goal is to **motivate students to involve themselves** in the unit/project.
It is a teasing activity.#br

The different themes addressed are:

- the objective of the unit/project, and why it takes place at this time of the year,

- the agenda of the unit/project,

- what this unit/project can bring them in the short term and in the long term, and how it can be useful in their professional life (backing up by using concrete examples).

#imageRight(images/tap.png, 50px, 0)

## animation 

A presentation **support** (found on the intranet) is often provided.
The kick-off is also a moment to exchange with students about any questions or concerns they may have about the unit/project.
It's a **very** good idea to demonstrate the project using a delivery that works very well and even have bonuses!

#imageCenter(images/kickoff.jpg, 400px)





#newpage

# Bootstrap
+--------------------------------------------------------------------------------------------------------------------------------------------------------------
| #color(COLOR_red,**planning**) 	#space(.58) 	short **after the Kick-off**
| #color(COLOR_red,**duration**)	#space(.6) 		**3 hours**
| #color(COLOR_red,**staff**)		#space(1.25) 	pedagogical **assistant**
| #color(COLOR_red,**audience**)	#space(.5) 		**small groups** of students
| #color(COLOR_red,**room**)		#space(1.1) 	**working rooms** with meeting tables
| #color(COLOR_red,**intranet**)	#space(.6) 		**subscription**, **project**, no mark, no time-slot, no rating scale
+--------------------------------------------------------------------------------------------------------------------------------------------------------------#br

Bootstraps' goal is to **give students the encouragement and motivation to succeed in their project**. It is the project launching activity. It involves a lot of coding.#br

The different possible focus points are:

- coding a set of central tool functions,
- writing a full set of functional tests to set up test-driven development,
- coding, testing and comparing different algorithms to cope with a critical point.

#warn(As much as possible\, we want students to produce robust and fully functional code.
To do so\, testing is a mandatory professional skill.
Bootstrap is the perfect time to set up a testing policy\, at least for advanced students.)

#imageRight(images/tap.png, 50px, 0)

## animation 

Most Bootstraps have an associated **subject** (provided on the intranet). 
If not, the subject is up to whether the students or the animator.
Some Bootstraps might come with automated tests.#br

Students should work in **small groups** (6/8 people), possibly splitting members of a same project group. They should **code, discuss and test**.
The animator oversees the activity, walking around tables and discussing the ongoing project and its progress.#br

#imageCenter(images/bootstrap.jpg, 350px)




#newpage

# Follow-Up
+--------------------------------------------------------------------------------------------------------------------------------------------------------------
| #color(COLOR_red,**planning**) 	#space(.58) 	possibly several sessions, roughly **1 every 2 weeks**
| #color(COLOR_red,**duration**)	#space(.6) 		**1 hour**
| #color(COLOR_red,**staff**)		#space(1.25) 	pedagogical **assistant**
| #color(COLOR_red,**audience**)	#space(.5) 		**small groups** of students\, if possible sorted by progress or interest
| #color(COLOR_red,**room**)		#space(1.1) 	**working rooms** with meeting tables
| #color(COLOR_red,**intranet**)	#space(.6) 		no subscription, no project, **mark**, **1h time-slot with 30 assistants**, **rating scale**
+--------------------------------------------------------------------------------------------------------------------------------------------------------------#br

Follow ups' goal is to **make sure students are automated-testing compliant**.
We do not want students with functional projects failing because of some unclear reasons.
There are 2 different ways of confronting students' code and testing:

- launching **automated testing** (with a special set of tests to avoid test sharing),

- launching **students tests** on the same platform as the one used for the final evaluation (the docker image is available).



#imageRight(images/tap.png, 50px, 0)

## animation 

Students must work in **small groups** (6/8 people), separating members of a same project group.
The groups must be sorted by level or progress. There are 2 different ways to proceed:

- Ideally, students should be **autonomous** and get organized to share, sort and rearrange their tests to build a full coherent set of tests. Then, they can evaluate their program, correct and refactor them.

- Otherwise, we use **our own set of tests**. 
The animator first launches the automated tests for all the students, without email sending.
He/she then sits at each table and gives the result of the first failed test to each student (**crash tests** mode), and writes this result in the comment section when filling the students grading scale.
Students should work on their project until they get the result of the testing. Then, they should work on the weak points of their program, and share their experience with others around the table.
Every student should have registered a time-slot beforehand (individually when possible).

#warn(Students are not supposed to code live against automated tests!
The animator should never execute tests on-demand.)

#imageCenter(images/followup.jpg, 300px)






#newpage

# Code review
+--------------------------------------------------------------------------------------------------------------------------------------------------------------
| #color(COLOR_red,**planning**) 	#space(.58) 	**every 3 weeks**
| #color(COLOR_red,**duration**)	#space(.6) 		**1 hour**
| #color(COLOR_red,**staff**)		#space(1.25) 	pedagogical **assistant**
| #color(COLOR_red,**audience**)	#space(.5) 		up to **12 students**
| #color(COLOR_red,**room**)		#space(1.1) 	**working rooms** with meeting tables
| #color(COLOR_red,**intranet**)	#space(.6) 		no subscription, no project, **mark**, **1h time-slot with 12 assistants**, **rating scale**
+--------------------------------------------------------------------------------------------------------------------------------------------------------------#br

The objective of the code review is not to evaluate competencies, but to give students constructive feedbacks about the points of improvement of their code.
Incidentally, it should also give other students a critical sense and an hindsight about coding.#br

This activity is a peer code review.
A table is consitued of roughly 6 students.
Every 10 minutes, one of them shows parts of his code to his peer so that they can comment it, ask question about it and mark on multiple points (readability, reusability, ...) defined in the grading scale.

#hint(The whole code need not being reviewed.
Part of it is enough.)
 
Students can only register once every 3 weeks (more or less) for a code review, in order to share the workload and avoid redundant reviews.

#hint(Students can review the project they want\, so they are not disabled by this distribution of sessions.)


#imageRight(images/tap.png, 50px, 0)

## animation

Students register alone, and can choose the project they want to present.
For solo projects, students have roughly 10 minutes each.
For group projects, if there is only one member from a given group, he/she has roughly 10 minutes.
If there are two members from the same group, they have roughly 20 minutes.
If there are three members from the same group, they have roughly 30 minutes.

#warn(There can not be more than 3 members from the same group around a table.)

At the beginning of the session, the 12 students are randomly (or not) dispatched around 2 tables by the animator.#br

Once the accorded time (10/20/30 minutes) is over, the assistant gathers and synthetises all the comments into the intra, using the generic Code Review grading scale.
He/she can use this opportunity to discuss with the students and ask some questions (based on the potential comments divergence).#br

With this mode of operation, it is possible to review 120 students in 10 sessions of one hour per assistant.
These sessions can be spread over three weeks and can be run at the same time if more than one assistant is available.



#imageCenter(images/code-review.jpg, 320px)



#newpage

# Review
+--------------------------------------------------------------------------------------------------------------------------------------------------------------
| #color(COLOR_red,**planning**) 	#space(.58) 	**every 3 weeks**
| #color(COLOR_red,**duration**)	#space(.6) 		**1 hour**
| #color(COLOR_red,**staff**)		#space(1.25) 	pedagogical **assistant**
| #color(COLOR_red,**audience**)	#space(.5) 		up to **6 groups**
| #color(COLOR_red,**room**)		#space(1.1) 	**lecture hall**
| #color(COLOR_red,**intranet**)	#space(.6) 		**subscription**, no project, **mark**, **1h time-slot with 6 assistants**, **rating scale**
+--------------------------------------------------------------------------------------------------------------------------------------------------------------#br

The objective of the review is to give students the opportunity to defend their best project.
It should encourager students to render at least one slick project every now and then, with nice features, testing and development methods.
Three points can be tackled:

- **testing**: global testing policy, functionnal and unit testing,  code coverage.
- **project management**: use of git, group management, planification, global organization...
- **bonus**: any valuable extra feature to be shown for evaluation or just for panache

Students can only register once every 3 weeks (more or less) for a review, in order to share the workload and avoid redundant reviews.

#imageRight(images/tap.png, 50px, 0)

## animation

Students register **as a project group**, and can choose the project they want to present.
They have roughlty 10 minutes to demonstrate the most interesting part of their project, covering the 3 mentionned points.
The pedagogical assistant comment and fill in the grading scale in the **Professionnal Competencies** unit.

The review are public, and all the students involved in a session have to assist the whole 1h-long-sesison.


#imageCenter(images/review.jpg, 320px)