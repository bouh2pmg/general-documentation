---
module:		Epitech Documentation
title:		Unit tests for C/C++
subtitle:	How to write them

author:		Louis-Philippe Baron and Jonathan Nau
version:	2.6

noFormalities:	true
---




# Prerequisite

Unit tests rely on [Criterion](https://github.com/Snaipe/Criterion) library.

To install Criterion, you can use the script *install_criterion.sh* (which can be found next to this document) or follow the instructions from the Criterion's website:
[http://criterion.readthedocs.io/en/master/setup.html#installation](http://criterion.readthedocs.io/en/master/setup.html#installation)


# Project tree

The only requirement for unit tests in your project tree is that the files containing the unit tests must be placed in a folder named `tests` at the root of the project. 

#hint(The subtree of this `tests` directory is up to you.)


#newpage

# Compiling and running using Criterion

To manually build your unit test manually you can do it using a command line such as:

#terminal(gcc -o unit_tests project.c tests/test_project.c -\\-coverage -lcriterion)

#warn(Criterion add it's own main, so none of the files present on the build line should contain a `main` function.)

Then execute the produced binary to see your test results.

#terminal(./unit_tests
[====] Synthesis: Tested: 4 | Passing: 4 | Failing: 0 | Crashing: 0)

## Evaluation at Epitech

Unless specified otherwise in the project subject, your tests will be built and launched using your own Makefile.

The `Makefile` used to compile the unit tests is **the Makefile at the root of your project directory**.#br

This Makefile **must** contains a rule named **tests_run** which *must* do two things:
- Build your unit-tests binary,
- Execute your unit-tests binary.

If everything is good you should see the number of test that passed and failed when executing **make tests_run**.

# Display code coverage of your tests

The execution of your unit-tests binary should create a bunch of `.gcda` and `.gcno` files. These can be used by tools to calculate the amount of code executed by your test.
This amount can be either in number of lines or number of branches (to check if every case of a condition has been properly tested).

#warn(To be able to compute a meaningful coverage you must always build your test with your code base (aka: all your C files).)

The tool we use to calculate your coverage is called [gcovr](https://gcovr.com/). We invite you to [install it](https://gcovr.com/installation.html).

You can use it like so (the test files are excluded from the coverage calculation):

#terminal(gcovr -\\-exclude tests/
```
------------------------------------------------------------------------------
                           GCC Code Coverage Report
Directory: .
------------------------------------------------------------------------------
File                                       Lines    Exec  Cover   Missing
------------------------------------------------------------------------------
test_project.c                                 7       7   100%
------------------------------------------------------------------------------
TOTAL                                          7       7   100%
------------------------------------------------------------------------------
```
#prompt gcovr -\\-exclude tests/ -\\-branches
```
------------------------------------------------------------------------------
                           GCC Code Coverage Report
Directory: .
------------------------------------------------------------------------------
File                                    Branches   Taken  Cover   Missing
------------------------------------------------------------------------------
test_project.c                                 6       6   100%
------------------------------------------------------------------------------
TOTAL                                          6       6   100%
------------------------------------------------------------------------------
```
)


# Tests files

The tests files only contain the tests, following this format:

```c
#\include <criterion/criterion.h>

Test(suite_name, test_name)
{
   ...
}
```
with *`suite_name + test_name`* unique.#br

The list of asserts is [here](https://github.com/Snaipe/Criterion/blob/514b4d820e2f8fb4daa2b95b69c981853656cb73/include/criterion/assert.h).
The most used are:

```c
// Passes if Expression is true
cr_assert(Expression, FormatString, ...);
// Passes if Expression is false
cr_assert_not(Expression, FormatString, ...);
// Passes if Actual == Expected
cr_assert_eq(Actual, Expected, FormatString, ...);
// Passes if Actual != Expected
cr_assert_neq(Actual, Expected, FormatString, ...);
```



#newpage

# Examples
Criterion maintainer has written many [example files](https://github.com/Snaipe/Criterion/blob/514b4d820e2f8fb4daa2b95b69c981853656cb73/samples).
Basic usage of Criterion can be found [here](https://github.com/Snaipe/Criterion/blob/514b4d820e2f8fb4daa2b95b69c981853656cb73/samples/asserts.c) and [here](https://github.com/Snaipe/Criterion/blob/514b4d820e2f8fb4daa2b95b69c981853656cb73/samples/asserts.cc).

Here is an example of a unit test file:
```c
#\include <criterion/criterion.h>

const char *str = "Hello world";
const int len = 11;

Test(utils, is\_str\_length\_equal\_to\_len\_v1)
{
   cr_assert(strlen(str) == len);
}

Test(utils, is\_str\_length\_equal\_to\_len\_v2)
{
   cr_assert_eq(strlen(str), len);
}

Test(utils, is\_str\_length\_equal\_to\_len\_v3)
{
   cr_assert_not(strlen(str) != len);
}
```

The 3 tests are doing the same thing with different syntaxes.
They check that the `"Hello world"` string has a length of 11 characters.#br

However the following test aborts:

```c
Test(utils, is\_str\_length\_different\_to\_len)
{
   cr_assert_neq(strlen(str), len);
}
```
