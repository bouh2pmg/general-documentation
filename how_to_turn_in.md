---
module:		Epitech Documentation
title:		How to turn in using BLIH
subtitle:	Manage Git repositories

author:		Jonathan Nau
version:	1.1

noFormalities:	true
---

# Prerequisites

#hint(If you are using the dump provided by the school\, these prerequisites are already installed on your computer.)

To create GIT repositories on our server and use them you need to install a few things:

- python 3.3, Git and OpenSSL (preferably install them using your distribution repositories)
- [BLIH](https://gitlab.com/EpitechContent/dump/blob/b130d3fcb7c2e49fc51db7541fdf5aba484e7aaa/blih.py) - Bocal Lightweight Interface for Humans

#warn(This document is mainly written for Linux users.
If you're using a different operating system, you may have some adaptations to make.)


# Generating a pair of ssh keys

In order to authenticate yourself on our server, we will use a pair of ssh key.

You need to generate a pair of **ssh keys** and upload your *public key* to our servers using the following command:

#terminal(blih -u *firstname.lastname@epitech.eu* sshkey upload /path/to/your/public/key)

Of course, replace *firstname.lastname@epitech.eu* by your own email address.

#hint(By default the generated ssh key should be located at *~/.ssh/id_rsa.pub*.)


# Setup your directory

To create a repository you have to use BLIH.

For instance, to create a repository called **my_repo**, use the following command:

#terminal(blih -u *firstname.lastname@epitech.eu* repository create my_repo)

Then you may want to add permissions on your repository to someone else.

#hint(For projects in the Epitech's curriculum you often have to add the permission to read to a user called **ramassage-tek** in order for us to be able to retrieve your project.)

So, to add the *read* permission to *ramassage-tek* on your *my_repo* repository, you have to execute this command:

#terminal(blih -u *firstname.lastname@epitech.eu* repository setacl my_repo ramassage-tek r)


# Use your repository

Once your repository is created using blih, you can use it locally with classic Git commands.

For instance, to clone the repository and thus having a local instance of it to work in:

#terminal(git clone git@git.epitech.eu:/*firstname.lastname
@epitech.eu*/*my_repo*)

#hint(For informations on Git and how to use it, please visit [https://git-scm.com/](https://git-scm.com/))