---
module:     Epitech Documentation
title:      C++ Coding Style
subtitle:   Inspired by Robert C. Martin's **Clean Code**

author:     Nicolas PHISTER
version:    1.27

noFormalities: true
---

The *Epitech Coding Style* is a set of rules, guidelines and programming conventions that has been created within the school, and that you are required to follow. 
It applies to:

* the organization of the turn-in directory,
* the denomination of identifiers,
* the overall layout (paragraphs),
* the local layout (lines),
* source files and headers,
* Makefiles and CMakeLists.

#warn(The *Coding Style* is a purely syntactic convention and can therefore not be used as an excuse if your program does not work!)

It is mandatory that any code written in C++ as part of Epitech projects abides by these rules, regardless of the year or unit.

#warn(Although the *Coding Style* is not required for all projects\, you must always sequence and structure your code!
Most of the rules in this *Coding Style* apply to all languages\, so it can be useful when you're working on projects in other languages.)

#hint(It's easier and quicker to follow the style from the beginning of a project than to adapt an existing code to it at the end.)

***

The existence of this *Coding Style* is justified by the need to standardize the writing of code within the school, in order to facilitate group work.
It is also an excellent way to encourage code structuring and clarity and thus facilitate:

* its reading,
* its debugging,
* its internal logic,
* its reuse,
* writing tests,
* adding new features...

#hint(When faced with a choice\, always choose the option that makes your code clearer\, ergonomic and flexible.)

However, if you provide a **complete, relevant, accurate justification with a long-term vision** (cleanliness, legibility, code flexibility, optimization, etc.), you can infringe some of the items in this *Coding Style*.

#warn(The relevance of this justification is left to the discretion of the proofreader\, so it is preferable to present a strong argumentation or to abstain.)

In case of uncertainty or ambiguity in the principles specified in this document, refer to your local education manager.

***

There are 3 levels of severity: **major** ![major](images/major.png), **minor** ![minor](images/minor.png), and **info** ![info](images/info.png).#br

There are many and many ways to produce unclean code.
Even though one cannot mention all of them in this document, they have to be respected.
Rules that are not explicitly defined in this document are called **implicit rules**.

#warn(Implicit rules are considered infos ![info](images/info.png))

***

#hint(This document is inspired by the [Linux Kernel Coding Style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html)\, and is freely adapted from Robert C. Martin's excellent book `Clean Code`.)

#hint(Some tools\, such as [Editor Config](http://editorconfig.org/)\, might simplify the task.)





#newpage

# D - Design

## D1 - Single Responsibility Principle

![minor](images/minor.png) Classes and functions should only do **one thing** and **not mix the different levels of abstraction**.

#hint(Check out the [single responsibility principle](https://en.wikipedia.org/wiki/Single_responsibility_principle): a class must only be changed for one reason.)

## D2 - Open/closed principle

![major](images/major.png) The only public elements of a class should be **member functions that need to be exposed**.
All **attributes should be private**.

#hint(Use getters and setters to provide access to attributes.
This will let you control what happens when an attribute is modified (you could notify `Observers`, for instance...).)

#hint(Check out the [open/closed principle](https://en.wikipedia.org/wiki/Open/closed_principle).)

## D3 - Liskov Substitution Principle

![minor](images/minor.png) Objects of a given type should be **replaceable with objects of its subtypes** without altering the correctness of the program. 
Therefore, **subtypes should adhere to the behavior of their parents**.

#hint(Check out the [Liskov substitution principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle).)


#newpage
## D4 - Dependency Inversion Principle

![minor](images/minor.png) **Interfaces** and **abstract classes** should be used as often as possible to avoid tying client code to its dependencies.

#hint(Check out the [dependency inversion principle](https://en.wikipedia.org/wiki/dependency_inversion_principle).)


## D5 - Constness

![minor](images/minor.png) Any member function that does not need to modify the calling instance should be **marked as `const`**.

## D6 - RAII

![minor](images/minor.png) A class should be responsible for the resources it handles, and **release them in its destructor**.#br

Upon construction, an object should be **ready for use**.
It should **not be necessary to call any `init` function or similar**.

```cpp
\#include "File.hpp"

int main()
{
        File f;

        f.open("file.txt");                                 // D6 violation
        std::cout << file.read() << std::endl;
        f.close();                                          // D6 violation

        return 0;
}

int main()
{
        File f("file.txt");                                 // OK

        std::cout << file.read() << std::endl;

        return 0;                                           // OK
}
```




#newpage

# N - Naming

## N1 - General

![major](images/major.png) All names should be **meaningful** and in **correct English**.

#hint(Abbreviations are tolerated to the extent that they can significantly reduce the size of the name without losing the meaning.)

## N2 - Files and folders

![major](images/major.png) The name of a file should be the same as the class defined in it, followed by the corresponding extension.
All file names and folders should respect **`PascalCase` convention**.

## N3 - Functions

![major](images/major.png) The name of a function should **define the task it executes** and should **contain a verb**.
All function names should respect the **`camelCase` convention**.

```cpp
int voyalsNb()                                         // N3 violation
{
        ...
}
int getVoyalsCount()                                  // OK
{
        ...
}
int djikstra()                                       // N3 violation
{
        ...
}
int searchShortestPath()                             // OK
{
        ...
}
```

#newpage
## N4 - Variables

![major](images/major.png) The name of a variable should **define its use** and should **contain a noun and no verbs**.
All variable names should respect the **`camelCase` convention**.#br

The names of **macros and global constants** and the content of **enums** should be written in **UPPER_CASE**, words separated by underscores.

```cpp
\#define IS\_PAGE\_ALIGNED(x) (!((x) & (PAGE_SIZE - 1)))       // OK
enum Arch {                                                 // OK
        I386 = 0,
        X86_64,
        ARM,
        ARM64,
        SPARC,
        POWERPC,
};
const float PI = 3.14159;                                   // OK
```

## N5 - Types

![major](images/major.png) The name of a type should **define what it represents** and should **contain a singular noun and no verbs**.
All type names should respect the **`PascalCase` convention**.

```cpp
using age = int;                                            // N4 violation
using Pixel = std::pair<int, int>;                          // OK
typedef Pixel Point;                                        // OK
```






#newpage

# O - File organization

## O1 - Contents of the turn-in directory

![major](images/major.png) Your turn-in directory should contain only **files required for compilation**.

#hint(This means no compiled (`.o`\, `.gch`\, `.a`\, `.so`\, ...)\, temporary or unnecessary files (`*~`\, `*#`\, `*.d`\, `toto`\, ...).
It also implies you should not turn in any `.c`\, `.h`\, `.cc` or `.hh` files.)

## O2 - File extensions

![minor](images/minor.png) Sources in a C++ program should only have **`.cpp` or `.hpp`** extensions.

## O3 - File coherence

![major](images/major.png) A source file should **match a class**, and group all the functions associated with that class.





#newpage

# G - Global scope

## G1 - File header

![major](images/major.png) The source files (`.cpp`, `.hpp`, `Makefile`, `CMakeLists.txt`...) should always start with the school's **standard header**.

```cpp
/*
** EPITECH PROJECT, \$YEAR
** \$NAME\_OF\_THE\_PROJECT
** File description:
**        No file there, just an Epitech header example
*/
```

## G2 - Separation of functions

![minor](images/minor.png) Inside a source file, **one and only one empty line** should separate function implementations.

## G3 - Indentation of pre-processor directives

![minor](images/minor.png) Pre-processor directives should be **indented according to the level of indirection**.

```cpp
\#ifndef WIN32PAGE_SIZE
	\#if defined(__i386__) || defined(__x86_64__)
		size_t PAGE_SIZE = 4096;
	\#else
		\#error "Unknown architecture"
	\#endif
\#endif
```

#warn(The L2 rule about indentation also applies to pre-processor directives.)

## G4 - Global variables

![major](images/major.png) Global variables should be **avoided** as much as possible.

#warn(This also applies to uses of the `Singleton` design pattern.)

## G5 - Static

![minor](images/minor.png) Global variables and functions that are not used outside of the compilation unit they belong to should be **marked with the `static` keyword**.

#warn(Be careful not to confuse the various uses of the `static` keyword.)

## G6 - Constants

![minor](images/minor.png) Any **non-trivial constant** should be defined by a constant, and preferably **constexpr**, global variable.

#warn(A variable is called constant if and only if it is accordingly marked as such with the `const` keyword.
Be careful\, this keyword follows some particular and sometimes surprising rules!)

```cpp
constexpr float PI = 3.14;                      // OK
```

## G7 - Template parameters

![minor](images/minor.png) Template parameters should be declared **on a separate line** from the function or class name.

```cpp
template<typename T, typename U>                            // OK
class Pair

template<typename T, typename U> class Pair                 // G7 violation
```

## G8 - Plain C functions

![major](images/major.png) Apart from `main`, a C++ program should contain **no plain C functions**.
All code should be **part of a class**.










#newpage

# F - Functions

## F1 - Number of lines

![major](images/major.png) The body of a function should be **as short as possible**.

#warn(If the body of a function exceeds **20 lines**\, it probably does too many tasks!)

```cpp
int main()                                  // this function is 2-line-long
{
        std::cout << "hello, world" << std::endl;
        return 0;
}
```

#quote(Linus Torvalds \, Linux Kernel Coding Style, The maximum length of a function is inversely proportional to the complexity and indentation level of that function. case-statement \, where you have lots of small things for a lot of different cases\, it's OK to have a longer function.)


## F2 - Number of columns

![major](images/major.png) Inside functions, the length of a line should not exceed **80 columns** (not to be confused with 80 characters!)

#warn(A tab represents a single character\, but several columns.)

## F3 - Argument count

![major](images/major.png) A function should not need more than **3 arguments**. 

#hint(Variadic functions are allowed\, but they should not be used to circumvent the limit of 3 parameters.)

## F4 - No arguments

![major](images/major.png) A function taking no parameters should **not take `void`** as argument in the function declaration.

```cpp
int getWordCount(void);                     // F4 violation
int getWordCount();                         // OK
```

## F5 - Comments inside a function

![minor](images/minor.png) There **should be no comments** within a function.
The function should be readable and self-explanatory, without further need for explanations.

#hint(The length of a function being inversely proportional to its complexity\, a complicated function should be short; so a header comment should be enough to explain it.)


## F6 - Polymorphism

![minor](images/minor.png) Functions should **not be marked `virtual` unless it is necessary**.
The **`override` and `final` keywords** should always be used when relevant.


## F7 - Callable parameters

![minor](images/minor.png) Functions should be transmitted as parameters using **an `std::function` or a pointer to member function**, not a function pointer.

## F8 - Copy elision

![major](images/major.png) Structures and objects should be passed as parameters using **a reference, not a pointer or a copy**, when a copy is not explicitly needed.

```cpp
void makeCoffee(Bottle &milk, int cups)     // OK
{
        doSomething();
}
```




#newpage

# L - Layout inside a function scope

## L1 - Code line content

![major](images/major.png) A line should correspond to **a single statement**.
Typical situations to avoid are:

* several assignments on the same line
* several semi-colons on the same line, used to separate several code sequences
* a condition and an assigment on the same line

```cpp
a = b = c = 0;                                  // L1 violation
++a; ++b;                                       // L1 violation
if ((i = getWordCount()) != 0)                  // L1 violation
if (cond) return ptr;                           // L1 violation
a = doSomething(), 5;                           // L1 violation
```

## L2 - Indentation

![minor](images/minor.png) Tabulations should be **8-space-wide** and used for indentation.
**Spaces should not be used** for indentation.

## L3 - Spaces

![minor](images/minor.png) Always place **a space after a comma or a keyword** (if it has arguments).#br

However, there should be no space between the name of a function and the opening parenthesis or after a unary operator.#br

All binary and ternary operators should be separated from the arguments by a space on each side.

#warn(`return` is a keyword but `sizeof` is a unary operator!)

```cpp
return(1);                                      // L3 violation
return 1;                                       // OK
break;                                          // OK
sum = term1 + 2 * term2;                        // OK
s = sizeof(File);                               // OK
```

## L4 - Curly brackets

![minor](images/minor.png) Opening curly brackets should be **at the end of their line**, except for functions where they must be placed alone on a new line.
Closing curly brackets should always be **alone on a new line**, except in the case of an `else` statement.

#hint(In the case of a single-line scope\, curly brackets are optional.)

```cpp
if (cond) { return ptr; }                       // L1 & L4 violation
while (cond) {
        doSomething();                          // OK
}
if (cond)                                       // L4 violation
{
        ...
} else {                                        // OK
        ...
}
if (cond)                                       // OK
        return ptr;
void printEnv()                                 // OK
{
        ...
}
void printEnv() {                               // L4 violation
        ...
}
int getWordCount() { return wordCount; }        // L4 violation
int getWordCount()                              // OK
{
        return wordCount;
}
```


## L5 - Variable declaration

![minor](images/minor.png) Variables should be declared **when they are needed**, and not necessarily at the beginning of the function.

Variables should be initialized as soon as they are declared.

**Only one variable** should be declared per line.

#newpage
## L6 - Line jumps

![minor](images/minor.png) Line breaks should be present when they **help make code comprehension easier**. 

```cpp
int replace(std::string &word, char from, char to)
{
        int count = 0;

        for (auto &c : word)
                if (c == from) {
                        c = to;
                        ++count;
                }

        return count;
}
```


#newpage

# V - Variables and types

## V1 - Pointers and references

![minor](images/minor.png) The pointer or reference symbol (`*` or `&`) should be **attached to the associated variable**, and not separated by any spaces.

#warn(This rule only applies in the context of a pointer or reference.)

```cpp
int* a;                                                     // V1 violation
int *a;                                                     // OK
int &a;                                                     // OK
int a = 3 * b;                                              // OK
int getWordCount(const std::string &str);                   // OK
```

## V2 - Plain Old Data Types

![minor](images/minor.png) Structures should be used to illustrate **PODs (plain old data types)**.
That is, a structure should not contain any internal logic (and therefore any functions that modify it).
If an object is to have a behavior of its own and keep a consistent logic among its members, **it must be a class**.

```cpp
class Point {                                               // V2 violation
public:
        int x;
        int y;
};

struct Point {                                              // OK
        int x;
        int y;
};

struct Parser {                                             // V2 violation
        Parser(const std::string &input);
        void parse(const std::function<void(std::string_view)> &func) const;
        ...

private:
        ...
};
```




#newpage

# C - Control structures

Unless otherwise specified\, all control structures are **allowed**.

## C1 - Conditional branching

![minor](images/minor.png) An `if` block **should not contain more than 3 branches**, excluding error handling.

#hint(Instead\, use an `std::unordered_map` of `std::function` objects.)

**Nested conditional branching** with a depth of 3 or more should be avoided.

#hint(If you need multiple levels of branching\, you probably need to refactor your function into sub-functions.)

```cpp
if (...)                                                    // OK
        doSomething();
else if (...)
        doSomethingElse();
else
        doSomethingMore();

if (...)                                                    // C1 violation
        doSomething();
else if (...)
        doSomethingElse();
else if (...)
        doSomethingMore();
else
        doOneLastThing();

while (...)                                                 // OK
        if (...)

while (...)                                                 // C1 violation
        for (...)
                if (...)
```

#hint(If you ever need to break this rule\, you could probably compute the [cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity) of your function to justify it...)

## C2 - Ternary expressions

![info](images/info.png) Ternary expressions are **allowed as long as they are kept simple and readable**, and they do not obfuscate code.

#warn(You should **never use nested or chained ternary expressions**.
Ternary expressions should **not be used to control program flow**.)

```cpp
const auto yearParity = (year % 2 == 0) ? Parity::Even : Parity::Odd;   // OK
return a > 0 ? a : 0;                                       // OK
int a = b > 10 ? c < 20 ? 50 : 80 : e == 2 ? 4 : 8;         // C2 violation
alreadyChecked ? goThere() : check();                       // C2 violation
```

## C3 - Goto

![minor](images/minor.png) Your code **should not contain the `goto` keyword**, especially because it can quickly participate in the creation of the infamous spaghetti code, which is completely illegible.

## C4 - For condition and iteration expressions

![major](images/major.png) `for` loop conditions and iteration expressions should be kept **as simple as possible** and should **not contain more than one instruction**.

```c++
for (int i = 0; i < getWordCount() + getSpaceCount(); ++i) { // C4 violation
        j = getKey(i);
}

const auto count = getWordCount() + getSpaceCount();
for (int i = 0; i < getWordCount() + getSpaceCount(); ++i, j = getKey(i))               // C4 violation

const auto count = getWordCount() + getSpaceCount();
for (int i = 0; i < count; ++i) {                           // OK
        j = getKey(i);
}
```

## C5 - Ranges

![minor](images/minor.png) Prefer **range-based for** loops to typical **for or while loops**, as these are clearer and easier to maintain.





#newpage

# A - Advanced

## A1 - Constant pointers

![minor](images/minor.png) Pointers and references should be declared **constant** if the referenced object is not to be modified.

## A2 - Scalar typing

![info](images/info.png) When choosing a **scalar type**, prefer the **most accurate type possible** according to the use of the data.

```cpp
int counter;                                                // A2 violation
unsigned int counter;                                       // OK

template<typename T>
unsigned int getObjSize(const T &obj);                      // A2 violation

template<typename T>
std::size_t getObjSize(const T &obj);                            // OK
```

## A3 - NULL

![info](images/info.png) `NULL` should **never be used**. Instead, `nullptr` offers **better type safety and code clarity**.

#hint(`null` pointers should be avoided as much as possible.)

## A4 - Auto

![info](images/info.png) The `auto` keyword should be used **as often as possible** as long as it **does not harm code clarity**.

#hint(DRY. Don't Repeat Yourself.)

```cpp
Koala k;
Animal &a = k;

Otter &o = dynamic_cast<Otter &>(a);                        // A4 violation
auto &o = dynamic_cast<Otter &>(a);                         // OK
```

## A5 - Casts

![minor](images/minor.png) Prefer the **most accurate cast possible**.
C-style casts should only be used **when converting scalar types**.

```cpp
Koala k;
Animal &a = k;

auto &o = (Otter &)a;                                       // A5 violation
auto &o = reinterpret_cast<Otter &>(a);                     // A5 violation
auto &o = static_cast<Otter &>(a);                          // OK
auto &o = dynamic_cast<Otter &>(a);                         // OK

double d = 42.42;
auto i = (int)d;                                             // OK
```

## A6 - C libraries

![minor](images/minor.png) The C standard library must be **avoided** as much as possible.
Its C++ counterpart should be used instead.

#hint(rand(3)\, time(2)... have their C++ counterparts. Use them!)

## A7 - Encapsulation

![minor](images/minor.png) When calling a function from a C library, it must be **encapsulated**.


## A8 - Conditional specialization
![info](images/info.png) If using **any standard as of C++ 17**, `if constexpr` should be used instead of `std::enable_if` for **compile-time conditions**.

## A9 - `using namespace`

![major](images/major.png) The `using namespace` keyword must not be used, as it defeats the purpose of creating namespaces.



#newpage

# H - Header files

## H1 - Content

![major](images/major.png) Header files should contain only:

* function prototypes,
* type declarations,
* macros,
* class and function templates,
* trivial getters and setters.

These elements should **only be found in header files**.

#hint(Header files can include other headers (if and only if it is necessary).)

## H2 - Include guard

![minor](images/minor.png) Headers should be protected from **double inclusion**.
The method and the conventions used are up to you.

## H3 - Macros

![minor](images/minor.png) Macros should **not be used instead of constants**.
Macros should match **a single statement**.

```cpp
\#define PI 3.14159                                          // H3 violation
constexpr float PI = 3.14159;                               // OK
\#define LOOP_COUNTER 0                                      // H3 violation
\#define DELTA(a, b, c) ((b) * (b) - 4 * (a) * (c))          // OK
\#define PRINT_NEXT(num) { num++; std::cout << num; }        // H3 violation
```




#newpage

# K - Classes


## K1 - Naming attributes

![major](images/major.png) Naming conventions for attributes should be **consistent**.

#warn(Using an **underscore in attribute names is authorized**.)

```cpp
Person::Person(const std::string &name)
    : _name(name)                                           // OK
{}

Person::Person(const std::string &name)
    : n_name(name)                                          // OK
{}

Person::Person(const std::string &name, const int &age)
    : _name(name), n_age(age)                               // K1 violation
{}

```

## K2 - Constructor list

![minor](images/minor.png) Constructor lists **should be used** instead of the constructor body.

```c++
Person::Person(const std::string &name)
{
    this->_name = name;                                     // K2 violation
}

Person::Person(const std::string &name)
    : _name(name)                                           // OK
{}
```

#newpage

## K3 - Class access modifiers

![info](images/info.png) The `private`, `public` and `protected` class access modifiers should be **aligned with the class keyword**.
A class' `public` members should be declared first, followed by its `protected` and `private` members.

```cpp
class Person {
public:                                                     // OK
        const std::string &getName() const
        {
            return _name;
        }

    private:                                                // K3 violation
        const std::string _name;
};

class Person {
private:                                                    // K3 violation
        const std::string _name;

public:
        ...
};
```

## K4 - Friend

![minor](images/minor.png) The `friend` keyword should **never be used**, unless it makes the code **significantly simpler** and does **not break encapsulation**.

## K5 - Operator overloads

![minor](images/minor.png) Operators should only be overloaded if they keep their **original semantics**.
For instance, the `<<` operator, when applied to an `std::ostream`, should **always serialize an object**.


## K6 - Type aliases

![info](images/info.png) Type aliases should be declared using **the `using` keyword** and not `typedef`.

```cpp
using Point = std::pair<int, int>;                          // OK
typedef std::pair<int, int> Point;                          // K6 violation
```




#newpage

# S - STL

## S1 - Containers

![minor](images/minor.png) As often as possible, the most accurate **STL container** should be used, according to the following diagram.

#imageCenterCorners(images/collections.png, 500px, 1)


## S2 - Smart pointers

![minor](images/minor.png) Heap-based objects should **always be managed by a smart pointer**.
`delete` should **never be called explicitly**.#br

Pointers should **always be managed by an `std::unique_ptr`** and not by a `shared_ptr`.


## S3 - Algorithms

![minor](images/minor.png) Standard algorithms should be used **as often as possible**.

#hint(Lambdas make algorithms very easy to use!)


#newpage

# E - Error handling

## E1 - Exit

![major](images/major.png) A function should **never call `exit` or any similar function** (such as `std::terminate`, `abort`...) directly.

## E2 - Return value

![minor](images/minor.png) The type of a function's return value should **always be in accordance with what the name of the function suggests**.

```cpp
bool addUser(const User &u);                            // E2 violation
void addUser(const User &u);                            // OK

char *strlen(const char *str);                          // E2 violation
std::size_t strlen(const char *str);                         // OK
```

## E3 - Exceptions

![minor](images/minor.png) When faced with an error that **cannot be conveyed by its return value**, a function should **throw an exception**.

```cpp
std::size_t strlen(const char *str)
{
    if (str == nullptr)
        return (size_t)-1;                                  // E3 violation
    ...
}

std::size_t strlen(const char *str)
{
    if (str == nullptr)
        throw std::runtime_error("Null parameter");         // OK
    ...
}
```

## E4 - Exception types

![minor](images/minor.png) An exception's type should **reflect the nature of the error**.

#hint(Standard types such as `std::out_of_range`\, `std::runtime_error` or `std::logic_error` generally suffice for this.)

## E5 - Noexcept

![info](images/info.png) When a function is not expected to ever throw an exception, it should **be marked as `noexcept`**, even if one of its statements may throw an exception.

```cpp
int getRandomNumber()                                       // E5 violation
{
    return 4; // chosen by fair dice roll.
              // guaranteed to be random.
}

int getRandomNumber() noexcept                              // OK
{
    return 4; // chosen by fair dice roll.
              // guaranteed to be random.
}

void addUser(const User &u) noexcept                        // OK
{
    users.push_back(u); // May throw, but not expected to
}
```

## E6 - Return points

![minor](images/minor.png) Functions should have a single "expected" exit point, and therefore **a single `return` expression.**.

#warn(Error cases are not expected exits.)
