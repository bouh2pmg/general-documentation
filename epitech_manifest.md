---
module:			Epitech Documentation
title:			Manifest
subtitle:		Epitech values

author:			Axelle Ziegler
version:		1.0

noFormalities:	true
debug:          false
---

#warn(Our values are **excellence**, **courage** and **solidarity**.)

## Excellence

Excellence means accepting that our work could only be regarded as accomplished if it were of **irreproachable quality**.
Excellence means **constantly looking for what can be improved**, rather than relying on what is done.
Excellence means acknowledging **everything we do not know**, and striving to learn it.

## Courage

Courage means **getting out of our comfort zone** to continually implement the necessary changes and improvements in our projects and in our ways of working.
Courage means **facing the difficulties** we encounter, and accepting to start in a simple way even those things that seem insurmountable.
Courage means **remaining humble** by agreeing to confront criticism head-on, and feeding on it.

## Solidarity

Solidarity means **respecting and communicating honestly** with everyone we work with, independently of the qualities and the flaws that they may have.
Solidarity means having the courage to **help**, and to  **ask for help**, always with benevolence.
Solidarity means **accepting the group's collective responsibility** rather than seeking to protect oneself.

#warn(We understand that **excellence** can not be achieved without **courage and solidarity**.)